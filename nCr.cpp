#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62

typedef long long ll;

using namespace std;

//ll r[1001][801];
//unsigned long long int j;
//ll CR(ll n ,ll k)
//{
//    if(r[n][k] != -1)
//        return r[n][k];
//    if(n== k || k==0)
//        return 1;
//    return r[n][k]=(CR(n-1, k-1) + CR(n-1 , k)) % j;
//}
//int main()
//{
//     ll t;
//     cin>>t;
//     Set(r,-1);
//     j= (pow(10, 9)+7);
//     while(t--)
//     {
//        ll n ,k  ;
//        cin>>n >> k;
//         if(n<k)
//            cout<<"0"<<endl;
//        else
//            cout<<CR(n ,k) <<endl;
//     }
//     return 0;
//
//}
//ll F[1002] ,q;
//ll CR(ll n , ll k )
//{
 //   F[0]=1;
//    For(i , 1 , n+1)
 //   {
   //     for(ll j = min(i , k); j>0 ;j--)
     //       F[j] = (F[j] + F[j-1] ) % q;

    //}
    //return F[k];

//}
ll CR(ll n , ll k){
    if(n-k < k) k = n-k;
    ll sum = n;
    For(i , 1 , k) sum*=(n-i) , sum/=i;
    return sum;
    
}
int main()
{
    ll t, n , k;
    cin>>t;
    q=pow(10 ,9)+7;
    while(t--)
    {
        Set(F ,0);
        cin>>n>>k;
        if(k>n)
            cout<<"0"<<endl;
        else
            cout<<CR(n, k)<<endl;
    }
}
// O(k)

//Note : commented is backtracking solution
